﻿using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace Utility.Helper
{
    public class DapperHelper
    {
        private const string CONNECTION_STRING = "Persist Security Info=False;User ID=rockman33017;Password=v9390025;Initial Catalog=Vocabulary;Server=220.135.127.166;Encrypt=True;TrustServerCertificate=true";
        private const int COMMAND_TIMEOUT = 30;

        public int Execute(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using var conn = new SqlConnection(CONNECTION_STRING);
            conn.Open();
            commandTimeout = commandTimeout.HasValue ? commandTimeout : COMMAND_TIMEOUT;
            return conn.Execute(sql, param, commandTimeout: commandTimeout, commandType: commandType);
        }

        public async Task<int> ExecuteAsync(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using var conn = new SqlConnection(CONNECTION_STRING);
            await conn.OpenAsync();
            commandTimeout = commandTimeout.HasValue ? commandTimeout : COMMAND_TIMEOUT;
            return await conn.ExecuteAsync(sql, param, commandTimeout: commandTimeout, commandType: commandType);
        }

        public T ExecuteScalar<T>(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using var conn = new SqlConnection(CONNECTION_STRING);
            conn.Open();
            commandTimeout = commandTimeout.HasValue ? commandTimeout : COMMAND_TIMEOUT;
            return conn.ExecuteScalar<T>(sql, param, commandTimeout: commandTimeout, commandType: commandType);
        }

        public async Task<T> ExecuteScalarAsync<T>(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using var conn = new SqlConnection(CONNECTION_STRING);
            await conn.OpenAsync();
            commandTimeout = commandTimeout.HasValue ? commandTimeout : COMMAND_TIMEOUT;
            return await conn.ExecuteScalarAsync<T>(sql, param, commandTimeout: commandTimeout, commandType: commandType);
        }

        public IEnumerable<T> Query<T>(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using var conn = new SqlConnection(CONNECTION_STRING);
            conn.Open();
            commandTimeout = commandTimeout.HasValue ? commandTimeout : COMMAND_TIMEOUT;
            return conn.Query<T>(sql, param, commandTimeout: commandTimeout, commandType: commandType);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using var conn = new SqlConnection(CONNECTION_STRING);
            await conn.OpenAsync();
            commandTimeout = commandTimeout.HasValue ? commandTimeout : COMMAND_TIMEOUT;
            return await conn.QueryAsync<T>(sql, param, commandTimeout: commandTimeout, commandType: commandType);
        }
    }
}