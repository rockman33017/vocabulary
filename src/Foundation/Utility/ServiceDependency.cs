﻿using Microsoft.Extensions.DependencyInjection;
using Utility.Helper;

namespace Utility
{
    public static class ServiceDependency
    {
        public static void InjectionUtility(this IServiceCollection services)
        {
            services.AddHelper();
        }

        public static void AddHelper(this IServiceCollection services)
        {
            services.AddSingleton<DapperHelper>();
        }
    }
}