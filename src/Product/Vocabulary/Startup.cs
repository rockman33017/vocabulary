﻿using Utility;

namespace Vocabulary
{
    public static class StartUp
    {
        public static void DependencyInjection(this IServiceCollection services)
        {
            services.InjectionUtility();
        }
    }
}